## redfin-user 13 TQ2A.230405.003.B2 9760227 release-keys
- Manufacturer: google
- Platform: lito
- Codename: redfin
- Brand: google
- Flavor: redfin-user
- Release Version: 13
- Kernel Version: 
- Id: TQ2A.230405.003.B2
- Incremental: 9760227
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:13/TQ2A.230405.003.B2/9760227:user/release-keys
- OTA version: 
- Branch: redfin-user-13-TQ2A.230405.003.B2-9760227-release-keys
- Repo: google_redfin_dump_10406
